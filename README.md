# Deploy Server here ====>  [![redeployazure.png](https://bitbucket.org/repo/Xderan/images/71391619-redeployazure.png)](https://portal.azure.com/#create/Microsoft.Template/uri/https%3A%2F%2Fraw.githubusercontent.com%2Flavett%2Fazure%2Fmaster%2Fnested2%2Fredeploy-main.json)

#### What is this repository for? ###

* This is a Azure template based on json to be used in Azure portal or through the resource manager using powershell or cli
* Deploying infra and windows server in one go
* Version 1.2

### How do I get set up? ###

* Press the deploy to Azure button above 
* Fill in the input properly
* Redo for more servers

### Who do I talk to? ###

* For questions regarding deploying or the setup contact [info@redeploy.se](mailto:info@redeploy.se) / +46 8 533 344 24